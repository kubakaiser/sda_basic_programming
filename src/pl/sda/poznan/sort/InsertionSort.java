package pl.sda.poznan.sort;

/**
 * Sortowanie przez wstawianie - analogia do ukladania kart
 * Na poczatku zakladamy z lewej strony ze mamy zbior jednoelementowy,
 * ktory juz jest posortowany (zbior 1 elementowy zawsze jest posortowany) -> i = 1
 * wybieramy pierwszy element ze zbioru nieposortowanego (zmienna j)
 * i probujemy wstawic do zbioru posortowanego
 * Jezeli napotkamy mniejszy element to zamieniamy elementy
 */
public class InsertionSort {
    public static void sort(int[] arr) {
        // zewnetrzna petla do przechodzenia zbioru nie posortowanego
        for (int i = 1; i < arr.length; i++) {
            // Przechodzimy przez zbior posortowany (od tylu)
            // wazne! j > 0 (bo porownujemy z elementem j-1)
            // petla do wstawiania do zzbioru posortowanego
            for (int j = i; j > 0; j--) {
                if (arr[j] < arr[j - 1]) {
                    int helper = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = helper;
                }
            }
        }
    }
}
