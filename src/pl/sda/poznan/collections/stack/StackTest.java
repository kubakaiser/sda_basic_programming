package pl.sda.poznan.collections.stack;

import org.junit.Test;

import static org.junit.Assert.*;

public class StackTest {

    Stack<String> stringStack = new Stack<>();

    @Test
    public void isEmpty() {

    }

    @Test
    public void shouldPushElement() {
        stringStack.push("First");
        stringStack.push("Second");
        stringStack.push("Third");

        assertEquals(3, stringStack.size());
    }


}